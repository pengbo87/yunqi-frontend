module.exports = {
  mode: "universal",
  /*
   **指定开发目录
   */
  srcDir: "src/",
  /*
   ** Headers of the page
   */
  head: {
    title: "云启资本",
    meta: [
      {
        charset: "utf-8",
      },
      {
        name: "viewport",
        content:
          "width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no",
      },
      {
        hid: "description",
        name: "description",
        content:
          "云启资本成立于2014年，一直坚持专注于先进技术和企业互联网方向的系统化投资布局，投资领域覆盖人工智能大数据、企业SaaS及服务、B2B供应链平台、智能网联、先进制造等行业。云启家族包括360金融(NASDAQ:QFIN)、英科医疗（SZ:300677)、一起写(被快手全资收购）、酷家乐、百布、PingCAP、冰鉴科技、擎朗智能、XTransfer、Zilliz、环世物流、找钢网、智齿科技、晓阳教育、德风科技、小胖熊、元戎启行、新石器、e换电等近100家优秀创业公司。",
      },
    ],
    link: [
      {
        rel: "icon",
        type: "image/png",
        href: "/logo.png",
      },
    ],
    script: [
      {
        type: "text/javascript",
        body: true,
        innerHTML: `document.write(unescape("%3Cspan id='cnzz_stat_icon_1279468136'%3E%3C/span%3E%3Cscript src='https://s9.cnzz.com/z_stat.php%3Fid%3D1279468136' type='text/javascript'%3E%3C/script%3E"));`,
      },
    ],
    __dangerouslyDisableSanitizers: ["script"],
  },
  /*
   ** Customize the progress-bar color
   */
  loading: {
    color: "#fff",
  },
  /*
   ** Global CSS
   */
  css: [
    // 项目里要用的 CSS 文件
    "@/assets/css/main.css",
  ],
  /*
   ** Plugins to load before mounting the App
   */
  plugins: ["~/plugins/i18n.js"],

  // 配置路由？？？
  router: {
    scrollBehavior: function(to, from, savedPosition) {
      if (to.hash) {
        const position = {};
        position.selector = to.hash;
        let routerArr = ["#index", "#portfolios", "#team", "#thinkTank"];
        if (routerArr.includes(to.hash)) {
          position.offset = {
            y: 60,
          };
        }
        return position;
      } else {
        return {
          x: 0,
          y: 0,
        };
      }
    },
    middleware: ["i18n", "auth"],
  },
  /*
   ** Nuxt.js modules
   */
  modules: [
    // Doc: https://bootstrap-vue.js.org/docs/
    "bootstrap-vue/nuxt",
    // Doc: https://axios.nuxtjs.org/usage
    "@nuxtjs/axios",
    "@nuxtjs/proxy",
  ],
  bootstrapVue: {
    bootstrapCSS: true, // Or `css: false`
    bootstrapVueCSS: true, // Or `bvCSS: false`
  },
  /*
   ** Axios module configuration
   ** See https://axios.nuxtjs.org/options
   */
  axios: {
    proxy: true,
  },
  proxy: {
    "/yunqi": {
      target: "http://www.yunqi.vc:8080",
      changeOrigin: true,
      secure: false,
      pathRewrite: {
        "^/yunqi": "/",
      },
    },
    "/local": {
      target: "http://localhost:3000",
      changeOrigin: true,
      secure: false,
      pathRewrite: {
        "^/local": "/",
      },
    },
  },
  /*
   ** Build configuration
   */
  build: {
    /*
     ** You can extend webpack config here
     */
    extend(config, ctx) {},
    babel: {
      plugins: [
        "transform-vue-jsx",
        [
          "component",
          {
            libraryName: "element-ui",
            styleLibraryName: "theme-chalk",
          },
        ],
      ],
    },
    // 开启打包分析
    // analyze: true,
    // assetFilter: function (assetFilename) {
    //   return assetFilename.endsWith('.js');
    // }
  },
};
