// import Vue from 'vue'
// import Antd from 'ant-design-vue/lib'
//
// export default () => {
//   Vue.use(Antd)
// }
import Vue from 'vue'
// import Antd from 'ant-design-vue/lib'
import Button from 'ant-design-vue/lib/button'
// import Icon from 'ant-design-vue/lib/icon'
import Menu from 'ant-design-vue/lib/menu'
import Layout from 'ant-design-vue/lib/layout'
import Select from 'ant-design-vue/lib/select'
import Table from 'ant-design-vue/lib/table'
import Input from 'ant-design-vue/lib/input'
import Popconfirm from 'ant-design-vue/lib/popconfirm'
import Form from 'ant-design-vue/lib/form'
import Col from 'ant-design-vue/lib/col'
import Row from 'ant-design-vue/lib/row'
import Pagination from 'ant-design-vue/lib/pagination'
import Checkbox from 'ant-design-vue/lib/checkbox'
import Upload from 'ant-design-vue/lib/upload'
import Radio from 'ant-design-vue/lib/radio'
// import {
//   Button,
//   Icon,
//   Menu,
//   Layout,
//   Select,
//   Table,
//   Input,
//   Popconfirm,
//   Form,
//   Col,
//   Row,
//   Pagination,
//   CheckBox,
//   Upload,
//   Radio
// } from 'ant-design-vue'

export default () => {
  Vue.use(Button)
  Vue.use(Menu)
  // Vue.use(Icon)
  Vue.use(Layout)
  Vue.use(Select)
  Vue.use(Table)
  Vue.use(Input)
  Vue.use(Popconfirm)
  Vue.use(Form)
  Vue.use(Row)
  Vue.use(Col)
  Vue.use(Pagination)
  Vue.use(Checkbox)
  Vue.use(Upload)
  Vue.use(Radio)
}
