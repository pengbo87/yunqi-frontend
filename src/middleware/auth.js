export default function ({
    isHMR,
    app,
    store,
    route,
    params,
    error,
    redirect
}) {

    if (route.fullPath.indexOf('/admin/') >= 0 && route.fullPath.indexOf !== '/admin/auth/login') {
        if (process.client) {
            setTimeout(() => {
                const logined = sessionStorage.getItem("login");
                if (logined !== "true") {
                    const subString = route.fullPath.slice(route.fullPath.indexOf('/admin/') + 7);
                    const directUrl = route.fullPath.replace(subString, 'auth/login');
                    return redirect(
                        directUrl
                    )
                }
            }, 200);
        }
    }
}