//去空格，去冒号，去空数据
class CheckString {
    static List(value) {
        let valueList = value.split("\n");
        valueList.map((d, index) => {
            valueList[index] = d.trim();
            var reg = new RegExp('"', "g");
            valueList[index] = valueList[index].replace(reg, "");
            valueList[index] = valueList[index].trim();
        });
        valueList = valueList.filter(d => {
            return d != "";
        });
        return valueList
    }
    static Number(value) {
        value = value.trim();
        var reg = new RegExp('"', "g");
        value = value.replace(reg, "");
        value = value.trim();
        return value
    }
}

export {
    CheckString
}