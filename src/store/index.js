export const state = () => ({
  locales: ['en', 'zh'],
  locale: 'zh',
  frontRoute: 'index',
  isIndex:true,
})
// 异步
export const mutations = {
  SET_LANG(state, locale) {
    if (state.locales.indexOf(locale) !== -1) {
      state.locale = locale
    }
  },
  setFrontRoute(state, frontRoute) {
    state.frontRoute = frontRoute
  },
  setFirst(state, isIndex) {
    state.isIndex = isIndex
  }
}

