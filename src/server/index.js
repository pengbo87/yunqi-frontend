const express = require('express')
const {
  Nuxt,
  Builder
} = require('nuxt')
const app = express()
const cookieParser = require('cookie-parser')
const bodyParser = require("body-parser");
const http = require('http');
const fs = require('fs'); // 引入fs模块


// Import and Set Nuxt.js options
const config = require('../../nuxt.config.js')
// config.dev = !(process.env.NODE_ENV === 'production')

const morgan = require('morgan');
const _ = require('lodash');
//morgan自定义日志日期时间
morgan.token('curr_date', function getCurrDate(req, res) {
  let curr_date = new Date();
  return '' + curr_date.getFullYear() + '-' +
    _.padStart(curr_date.getMonth() + 1, 2, '0') + '-' +
    _.padStart(curr_date.getDate(), 2, '0') + ' ' +
    _.padStart(curr_date.getHours(), 2, '0') + ':' +
    _.padStart(curr_date.getMinutes(), 2, '0') + ':' +
    _.padStart(curr_date.getSeconds(), 2, '0');
})
app.use(morgan('[:curr_date] :remote-addr :remote-user :method :url HTTP/:http-version :status :res[content-length] - :response-time ms'));
//app.use(morgan('short'));
function deleteall(path) {
    var files = [];
    if (fs.existsSync(path)) {
        files = fs.readdirSync(path);
        files.forEach(function (file, index) {
            var curPath = path + "/" + file;
            if (fs.statSync(curPath).isDirectory()) { // recurse
                deleteall(curPath);
            } else { // delete file
                fs.unlinkSync(curPath);
            }
        });
        fs.rmdirSync(path);
        console.log('------清空文件夹完毕------');
    }
}

//解析post请求获取的参数
app.use('/node/', bodyParser.json());
app.use('/node/', bodyParser.urlencoded({
  extended: false
}));

//中间件调用cookie
app.use(cookieParser());

app.put('/node/cookie', (req, res) => {
  if (req.body.name && req.body.val) {
    res.cookie(req.body.name, req.body.val, {
      httpOnly: true
    });
    res.send(resData(true, ''));
  } else {
    res.send(resData(false, '', '缺少参数'));
  }
});

//获取cookie
app.get('/node/cookie', (req, res) => {
  if (req.cookies[req.query.name]) {
    res.send(resData(true, req.cookies[req.query.name]));
  } else {
    res.send(resData(false, '', '获取不到值'));
  }
});

//删除cookie
app.delete('/node/cookie', (req, res) => {
  if (req.query.name) {
    res.clearCookie(req.query.name, {
      maxAge: 0
    });
    res.send(resData(true, ''));
  } else {
    res.send(resData(false, '', '缺少参数'));
  }
});


//批量------设置cookie
app.put('/node/batch/cookie', (req, res) => {
  if (JSON.stringify(req.body) == '{}') {
    res.send(resData(false, '', '缺少参数'));
  } else {
    for (let i in req.body) {
      res.cookie(i, req.body[i], {
        httpOnly: true
      });
    }
    res.send(resData(true, ''));
  }
});

//批量------获取cookie
app.get('/node/batch/cookie', (req, res) => {
  if (!req.query.name || req.query.name.length == 0) {
    res.send(resData(false, '', '缺少参数'));
  } else {
    let obj = {};
    req.query.name.map((item) => {
      if (req.cookies[item]) {
        obj[item] = req.cookies[item];
      }
    })
    res.send(resData(true, obj));
  }
});

//批量------删除cookie
app.delete('/node/batch/cookie', (req, res) => {
  if (!req.query.name || req.query.name.length == 0) {
    res.send(resData(false, '', '缺少参数'));
  } else {
    req.query.name.map((item) => {
      if (req.cookies[item]) {
        res.clearCookie(item, {
          maxAge: 0
        });
      }
    })
    res.send(resData(true, ''));
  }
});


function start() {
  // Init Nuxt.js
  const isProd = process.env.NODE_ENV === 'production';

  process.env.HOST = '0.0.0.0';
  process.env.PORT = isProd ? 8090 : 3000;

  const nuxt = new Nuxt(config)

  const {
    host,
    port
  } = nuxt.options.server

  // Build only in dev mode

  if (!isProd) {
    deleteall('./.nuxt');
    //  const builder = new Builder(nuxt);
    //  builder.build();
    new Builder(nuxt).build()
      .catch((error) => {
        console.error(error)
        process.exit(1)
      })
  }

  // Give nuxt middleware to express
  app.use(nuxt.render)

  //设置最多绑定函数事件处理数量
  var server = http.createServer(app);
  server.setMaxListeners(0);
  server.listen(process.env.PORT, process.env.HOST);

  console.log(`Server is listening on http://${process.env.HOST}:${process.env.PORT}`);
}
start()
